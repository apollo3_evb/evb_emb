#include "hxsc.h"
#include "tommath.h"
#include "tomcrypt.h"


#ifdef HXSC_DEBUG   /* Debug Only */

/*---------------------------------------------------------------*/
/* Global variable                                               */
/*---------------------------------------------------------------*/
clock_t              g_dbg_clock_start;
int                  g_counter_malloc;
int                  g_counter_calloc;
int                  g_counter_realloc;
int                  g_counter_free;
/*---------------------------------------------------------------*/
/* Start timer and Measure operation                             */
/*---------------------------------------------------------------*/
void hxsc_debug_clock_start()              
{
   //yaainteg g_dbg_clock_start = clock();
}
void hxsc_debug_passed_time(char *label)   
{
     clock_t passed_time;
     //yaainteg passed_time = clock() - g_dbg_clock_start;
     
     //HXSC_PRINTF("%s took: %d ms\n", label, passed_time);
}


/*---------------------------------------------------------------*/
/* Count Malloc/calloc/Realloc/Free                              */
/*---------------------------------------------------------------*/
void hxsc_debug_inc_counter_malloc()   {g_counter_malloc++; };
void hxsc_debug_inc_counter_calloc()   {g_counter_calloc++; };
void hxsc_debug_inc_counter_realloc()  {g_counter_realloc++;};
void hxsc_debug_inc_counter_free()     {g_counter_free++;   };

/*---------------------------------------------------------------*/
/* Print Statistics                                              */
/*---------------------------------------------------------------*/
void hxsc_memmgr_print_statistics()
{
   HXSC_PRINTF("g_counter_malloc =%-4d\n", g_counter_malloc);
   HXSC_PRINTF("g_counter_calloc =%-4d\n", g_counter_calloc);
   HXSC_PRINTF("g_counter_realloc=%-4d\n", g_counter_realloc);
   HXSC_PRINTF("g_counter_free   =%-4d\n", g_counter_free);
}

#ifdef THIS_IS_THE_OLD_ONE
    void hxsc_memmgr_print_statistics()
    {
        int inx;
        int bufsize;
        int pool_size;
        int num_allocated_bufs;
        
        for(inx=0; inx < HXSC_MEMMGR_NUM_OF_POOLS; inx++)
        {
            hxsc_memmgr_get_pool_counts(inx, &bufsize, &pool_size, &num_allocated_bufs);
            HXSC_PRINTF("Pool#%d PoolSize=%-4d BufferSize=%-4d Free=%-4d Allocated=%-4d\n", 
                         inx+1, pool_size, bufsize, pool_size-num_allocated_bufs, num_allocated_bufs);
        }
        
        HXSC_PRINTF("Total malloc:%d Total Free: %d\n", dbg_malloc_counter, dbg_free_counter);
    }
#endif
 
#endif   /* Debug Only */