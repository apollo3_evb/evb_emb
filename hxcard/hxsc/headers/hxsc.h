/* hxsc.h - all inclusive include file
 */

#ifndef HXSC_H_
#define HXSC_H_

#include "am_util.h"

#include <tomcrypt.h>

/*--------------------------------------------------------*/
/* DEBUG Option                                           */
/*--------------------------------------------------------*/
#if defined(_WIN32)
  #define HXSC_DEBUG
#else
  #define HXSC_DEBUG
#endif


/*--------------------------------------------------------*/
/* DEBUG Printing                                         */
/*--------------------------------------------------------*/
#if defined(_WIN32) && defined(HXSC_DEBUG)
     #define HXSC_PRINTF       printf
     #define HXSC_PRINTERR     printf
#elif defined(HXSC_DEBUG)
     #define HXSC_PRINTF       am_util_stdio_printf
     #define HXSC_PRINTERR     am_util_stdio_printf
#else
     #define HXSC_PRINTF       /* Do Nothing */
     #define HXSC_PRINTERR     am_util_stdio_printf
#endif

/*--------------------------------------------------------*/
/* Return Codes                                           */
/*--------------------------------------------------------*/
#define HXSC_RET_OK        0   /* OK                      */
#define HXSC_RET_BAD_INX   1   /* Bad Index               */
#define HXSC_RET_KEY_SIZE  2   /* Bad key size            */

/*--------------------------------------------------------*/
/*  Buffers size                                          */
/*--------------------------------------------------------*/
#define HXSC_HASH_SIZE              32   /* HASH256 Size                               */
#define HXSC_MAX_BTC_PRV_KEY_SIZE   128  /* Max size of Private key of a BITCOIN       */
#define HXSC_MAX_BTC_PUB_KEY_SIZE   128  /* Max size of Public key of a BITCOIN        */
#define HXSC_MAX_TX_SIGNATURE_SIZE  256  /* Max size of the signature of a transaction */

/*--------------------------------------------------------*/
/* Crypto Function Prototypes                             */
/*--------------------------------------------------------*/
int hxsc_init(void);
int hxsc_authenticate(int seed_inx, unsigned char *nounce, int nounce_len, unsigned char output_hash[]);   

/*--------------------------------------------------------*/
/* Memory Management Function Prototypes                  */
/*--------------------------------------------------------*/
void *hxsc_malloc(size_t size);
void *hxsc_realloc(void *p, size_t newsize);
void *hxsc_calloc(size_t count, size_t size);
void hxsc_free(void *p);   

/*--------------------------------------------------------*/
/* Function Prototypes                                     */
/*--------------------------------------------------------*/
#if defined(HXSC_DEBUG)
      void hxsc_debug_print_buf(const char *lable, const unsigned char buf[], int len, char *delimiter);
      void hxsc_print_key(rsa_key *p_key);
      
      void hxsc_memmgr_print_statistics();
      void hxsc_debug_clock_start();              
      void hxsc_debug_passed_time(char *label);
#ifdef yaainteg
      void HXSC_DBG_INC_MALLOC();
      void HXSC_DBG_INC_CALLOC(); 
      void HXSC_DBG_INC_REALLOC(); 
      void HXSC_DBG_INC_FREE(); 
#endif
        extern int                  dbg_malloc_counter;
   extern int                  dbg_realloc_counter;
   extern int                  dbg_calloc_counter;
   extern int                  dbg_free_counter;
   #define HXSC_DBG_INC_MALLOC()        dbg_malloc_counter++;
   #define HXSC_DBG_INC_REALLOC()        dbg_realloc_counter++;
   #define HXSC_DBG_INC_CALLOC()        dbg_calloc_counter++;
   #define HXSC_DBG_INC_FREE()          dbg_free_counter++;
      
#else  
      #define hxsc_debug_print_buf(a,b,c,d)    /* Do nothing */
      #define hxsc_print_key(rsa_key *p_key)   /* Do nothing */
      
      #define hxsc_memmgr_print_statistics()   /* Do nothing */ 
      #define hxsc_debug_clock_start()         /* Do nothing */
      #define hxsc_debug_passed_time(a)        /* Do nothing */
      
      #define hxsc_debug_inc_counter_malloc()  /* Do nothing */
      #define hxsc_debug_inc_counter_calloc()  /* Do nothing */
      #define hxsc_debug_inc_counter_realloc() /* Do nothing */
      #define hxsc_debug_inc_counter_free()    /* Do nothing */

#endif

#endif