#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <hxsc.h>
#include "tomcrypt.h"

/*------------------------------------------------------------*/
/* Debug Printing                                            */
/*------------------------------------------------------------*/
#if defined(_WIN32)
  #define HXSC_DEBUG
#endif

#ifdef HXSC_DEBUG
//    #define HXSC_PRINTF           printf
   
#else
    #define HXSC_PRINTF
   
#endif

#ifdef yaainteg
#ifdef HXSC_DEBUG
   int                  dbg_malloc_counter;
   int                  dbg_free_counter;
   #define HXSC_DBG_INC_MALLOC()        dbg_malloc_counter++;
   #define HXSC_DBG_INC_FREE()          dbg_free_counter++;
#else
   #define HXSC_DBG_INC_MALLOC()
   #define HXSC_DBG_INC_FREE()
#endif
#endif
#define HXSC_DEUBUG 1
#ifdef HXSC_DEBUG
int                  dbg_malloc_counter;
int                  dbg_realloc_counter;
int                  dbg_calloc_counter;
int                  dbg_free_counter;
#define HXSC_DBG_INC_MALLOC()        dbg_malloc_counter++;
#define HXSC_DBG_INC_REALLOC()        dbg_realloc_counter++;
#define HXSC_DBG_INC_CALLOC()        dbg_calloc_counter++;
#define HXSC_DBG_INC_FREE()          dbg_free_counter++;
#endif 


/*------------------------------------------------------------*/
/* True/False Macros                                          */
/*------------------------------------------------------------*/
#define HXSC_TRUE             1
#define HXSC_FALSE            0
#define HXSC_ERROR_REQUESTED_SIZE_TOO_BIG       NULL

/*------------------------------------------------------------*/
/* Supported buffers size                                     */
/*------------------------------------------------------------*/
#define HXSC_MEMMGR_BUFSIZE1  16
#define HXSC_MEMMGR_BUFSIZE2  32
#define HXSC_MEMMGR_BUFSIZE3  300
#define HXSC_MEMMGR_BUFSIZE4  1200

/*------------------------------------------------------------*/
/* Supported Pool size                                        */
/*------------------------------------------------------------*/
#define HXSC_MEMMGR_POOLSIZE1  450
#define HXSC_MEMMGR_POOLSIZE2  150
#define HXSC_MEMMGR_POOLSIZE3  150
#define HXSC_MEMMGR_POOLSIZE4  40

/*------------------------------------------------------------*/
/* The following buffer sizes are supported                   */
/*------------------------------------------------------------*/
const int ghxsc_memmgr_buf_sizes[] = {HXSC_MEMMGR_BUFSIZE1, HXSC_MEMMGR_BUFSIZE2, HXSC_MEMMGR_BUFSIZE3, HXSC_MEMMGR_BUFSIZE4};

/*------------------------------------------------------------*/
/* Number of supported pools                                  */
/*------------------------------------------------------------*/
#define HXSC_MEMMGR_NUM_OF_POOLS  sizeof(ghxsc_memmgr_buf_sizes)/sizeof(ghxsc_memmgr_buf_sizes[0])

/*----------------------------------------------------------*/
/* The header of every buffer                               */
/*----------------------------------------------------------*/
#define HXSC_MEMMGR_BUF_HEADER_SIZE 4      /* Header of a buffer - 4 byte?    */
#define HXSC_BUFFER_MASK_ALLOCATED 0x80    /* Header flag - allocated or free */

/*------------------------------------------------------------*/
/* The following buffer pool size are supported               */
/*------------------------------------------------------------*/
const int ghxsc_memmgr_pool_sizes[] = {HXSC_MEMMGR_POOLSIZE1, HXSC_MEMMGR_POOLSIZE2, HXSC_MEMMGR_POOLSIZE3, HXSC_MEMMGR_POOLSIZE4};

/*------------------------------------------------------------*/
/* The heap size                                              */
/*------------------------------------------------------------*/
#define HXSC_MEMMGR_HEAP_SIZE ((HXSC_MEMMGR_BUFSIZE1+HXSC_MEMMGR_BUF_HEADER_SIZE) * HXSC_MEMMGR_POOLSIZE1) + \
                              ((HXSC_MEMMGR_BUFSIZE2+HXSC_MEMMGR_BUF_HEADER_SIZE) * HXSC_MEMMGR_POOLSIZE2) + \
                              ((HXSC_MEMMGR_BUFSIZE3+HXSC_MEMMGR_BUF_HEADER_SIZE) * HXSC_MEMMGR_POOLSIZE3) + \
                              ((HXSC_MEMMGR_BUFSIZE4+HXSC_MEMMGR_BUF_HEADER_SIZE) * HXSC_MEMMGR_POOLSIZE4)
unsigned long yaaLen = HXSC_MEMMGR_HEAP_SIZE;
/*------------------------------------------------------------*/
/* The following buffer sizes are supported                   */
/*------------------------------------------------------------*/
const unsigned char *ghxsc_memmgr_pool_pointer[HXSC_MEMMGR_NUM_OF_POOLS] = {NULL, NULL, NULL, NULL};

/*------------------------------------------------------------*/
/* The following is the actual heap                           */
/*------------------------------------------------------------*/
unsigned char ghxsc_memmgr_heap[HXSC_MEMMGR_HEAP_SIZE];

/*------------------------------------------------------------*/
/* Init the heap                                              */
/*------------------------------------------------------------*/
void hxsc_memmgr_init()
{
    int inx;
    int bufsize;
    int pool_size;
    unsigned char *p_pool_location;

    yaaLen++;
    memset((void *)ghxsc_memmgr_heap, 0x0, HXSC_MEMMGR_HEAP_SIZE);

    p_pool_location = &ghxsc_memmgr_heap[0];
    for(inx=0; inx < HXSC_MEMMGR_NUM_OF_POOLS; inx++)
    {
        ghxsc_memmgr_pool_pointer[inx] = p_pool_location;
        pool_size = ghxsc_memmgr_pool_sizes[inx];
        bufsize = ghxsc_memmgr_buf_sizes[inx];
        p_pool_location += pool_size * (bufsize+HXSC_MEMMGR_BUF_HEADER_SIZE);
    }
}


/*------------------------------------------------------------*/
/* Find index of pool to allocate from                        */
/* according to the requested size                            */
/*------------------------------------------------------------*/
const unsigned char *hxsc_memmgr_find_pool_and_index(int required_buf_size, int *bufIndex)
{
    unsigned char inx;
    int bufsize;

    for (inx = 0; inx < HXSC_MEMMGR_NUM_OF_POOLS; inx++)
    {
        bufsize = ghxsc_memmgr_buf_sizes[inx];
        if (required_buf_size <= bufsize)
        {
            *bufIndex = inx;
            //HXSC_PRINTF("Pool found for buffer size=%d. PoolInx=%d PoolSize=%d BufSize=%d\n", required_buf_size, inx, *p_pool_size, bufsize);
            return ghxsc_memmgr_pool_pointer[inx];
        }
    }
    HXSC_PRINTF("No pool found that this required buffer size (%d) can be allocated from\n", required_buf_size);
    return NULL;
}
/*------------------------------------------------------------*/
/* Find a pool to allocate from                               */
/*------------------------------------------------------------*/
static const unsigned char *hxsc_memmgr_find_pool(int required_buf_size, int *p_pool_size, int *p_bufsize)
{
    int inx;
    int bufsize;
    const unsigned char *new;

    new = hxsc_memmgr_find_pool_and_index(required_buf_size, &inx);
    if (NULL != new)
    {
        *p_bufsize = ghxsc_memmgr_buf_sizes[inx];
        *p_pool_size = ghxsc_memmgr_pool_sizes[inx];
        return new;
    }
    else
    {
        HXSC_PRINTF("No pool found that this required buffer size (%d) can be allocated from\n", required_buf_size);
        return NULL;
     }
}

/*------------------------------------------------------------*/
/* Is a buffer free?                                          */
/*------------------------------------------------------------*/
static int hxsc_memmgr_is_buffer_free(const unsigned char *p_buffer)
{
    if(p_buffer[0] & HXSC_BUFFER_MASK_ALLOCATED)
    {
        return HXSC_FALSE;
    }
    return HXSC_TRUE;
}
/*------------------------------------------------------------*/
/* Free a buffer                                              */
/*------------------------------------------------------------*/
static void hxsc_memmgr_free_buffer(unsigned char *p_buffer)
{
    if(p_buffer[0] & HXSC_BUFFER_MASK_ALLOCATED)
    {
        p_buffer[0] &= ~HXSC_BUFFER_MASK_ALLOCATED;
        return;
    }
    HXSC_PRINTF("Error: Trying to free a buffer which is not allocated: mask=%x p=%X\n", p_buffer[0], p_buffer);
}

/*------------------------------------------------------------*/
/* Allocate a buffer                                              */
/*------------------------------------------------------------*/
static void hxsc_memmgr_allocate_buffer(unsigned char *p_buffer)
{
    if(p_buffer[0] & HXSC_BUFFER_MASK_ALLOCATED)
    {
        HXSC_PRINTF("Error: Trying to allocate a buffer which is already allocated: mask=%x p=%X\n", p_buffer[0], p_buffer);
        return;
    }
    p_buffer[0] |= HXSC_BUFFER_MASK_ALLOCATED;
    
}

/*------------------------------------------------------------*/
/* Find an available buffer in a pool                         */
/*------------------------------------------------------------*/
const unsigned char *hxsc_memmgr_find_available_buffer(const unsigned char *p_pool_pointer, int pool_size, int bufsize)
{
    int inx;
    unsigned char *p_buffer;

    p_buffer = (unsigned char *)&p_pool_pointer[0];
    for(inx=0; inx < pool_size; inx++)
    {
        if(hxsc_memmgr_is_buffer_free(p_buffer))
        {
            //HXSC_PRINTF("The buffer %d is free (out of %d buffers). We are allocating it.\n", inx, pool_size);
            hxsc_memmgr_allocate_buffer(p_buffer);
            return p_buffer;
        }
        p_buffer = p_buffer + bufsize + HXSC_MEMMGR_BUF_HEADER_SIZE;
    }

    HXSC_PRINTF("Error: Failed to allocate a buffer from pool %X (pool size=%d)\n", p_pool_pointer, pool_size);
    return NULL;
}

/*------------------------------------------------------------*/
/* Find buffer size given a ptr to buffer                     */
/*------------------------------------------------------------*/
int hxsc_memmgr_whats_my_bufsize (void * ptr)
{
    int inx;
    int bufsize;

    if ((ptr < ghxsc_memmgr_pool_pointer[0]) || 
        (ptr> (ghxsc_memmgr_pool_pointer[HXSC_MEMMGR_NUM_OF_POOLS-1]
               +(ghxsc_memmgr_pool_sizes[HXSC_MEMMGR_NUM_OF_POOLS-1]*ghxsc_memmgr_buf_sizes[HXSC_MEMMGR_NUM_OF_POOLS-1]))))
    {
        //out of range of our buffers pool
        ptr = NULL;
        bufsize = (int) NULL; //0 length
    }
    for (inx = 1; inx < HXSC_MEMMGR_NUM_OF_POOLS; inx++)
    {        
        if (ptr < ghxsc_memmgr_pool_pointer[inx])
        {
            break;
        }
    }
    if (inx >= HXSC_MEMMGR_NUM_OF_POOLS)
    {
        bufsize = ghxsc_memmgr_buf_sizes[HXSC_MEMMGR_NUM_OF_POOLS-1];
    }
    else
    {
        bufsize = ghxsc_memmgr_buf_sizes[inx-1];
    }
    
    return bufsize;
}

/*------------------------------------------------------------*/
/* Allocate memory                                            */
/*------------------------------------------------------------*/
void *hxsc_malloc(size_t mem_size)
{
    const unsigned char        *p_pool;
    int                         pool_size;
    int                         bufsize;
    const unsigned char              *p_buffer;

    p_pool = hxsc_memmgr_find_pool(mem_size, &pool_size, &bufsize);
    if(p_pool == NULL)
    {
        return NULL;
    }
    p_buffer = hxsc_memmgr_find_available_buffer(p_pool, pool_size, bufsize);
    if(p_buffer == NULL)
    {
        return NULL;
    }
    else
    {
        HXSC_DBG_INC_MALLOC();
    }
    return (void *)&p_buffer[HXSC_MEMMGR_BUF_HEADER_SIZE];  /* Skip The header */
}


/*------------------------------------------------------------*/
/* Free memory                                                */
/*------------------------------------------------------------*/
void hxsc_free(void *p_mem)
{
    unsigned char              *p_buffer;

    p_buffer = p_mem;
    p_buffer -= HXSC_MEMMGR_BUF_HEADER_SIZE;
    hxsc_memmgr_free_buffer(p_buffer);
    HXSC_DBG_INC_FREE();
}

/*------------------------------------------------------------*/
/* Calloc memory                                                */
/*------------------------------------------------------------*/
void *hxsc_calloc(size_t num_elems, size_t elem_size)
{
  int tot_size = num_elems * elem_size;
  
    unsigned char *ptr = hxsc_malloc(tot_size);
    if (NULL != ptr)
    {
      memset(ptr, 0, tot_size);
      HXSC_DBG_INC_CALLOC();
    }
    return ptr;
}

void *hxsc_realloc(void *p_mem, size_t new_size)
{
    void *new;
    const unsigned char * poolPtr;
    size_t newBufSize, currentBufSize;
    int inx;
     
    if (!p_mem)
    {
        // if input ptr is NULL, realloc is equivalent to malloc
        new = XMALLOC(new_size);
        if (!new) { goto error; }
    }
    else 
    {        
        poolPtr = hxsc_memmgr_find_pool_and_index(new_size, &inx);
        if (!poolPtr) { goto error; }

        newBufSize = ghxsc_memmgr_buf_sizes[inx];
        currentBufSize = hxsc_memmgr_whats_my_bufsize(p_mem);
        new = XMALLOC(new_size);
        if (!new) { goto error; }
        //copy everything from old buffer without overrunning the new buffer
        if (currentBufSize < newBufSize)
        {            
            memcpy(new, p_mem, currentBufSize);
        }
        else
        {
            memcpy(new, p_mem, newBufSize);
        }
        HXSC_DBG_INC_REALLOC();
        XFREE(p_mem);
    }
    return new;
error:
    return NULL;
}

#ifdef HXSC_DEBUG
   int                  dbg_malloc_counter;
   int                  dbg_free_counter;
   #define HXSC_DBG_INC_MALLOC()        dbg_malloc_counter++;
   #define HXSC_DBG_INC_FREE()          dbg_free_counter++;

    void hxsc_memmgr_count_buffers(const unsigned char *p_pool_pointer, int pool_size, int bufsize, int *p_num_allocated_bufs)
    {
        int inx;
        unsigned char *p_buffer;

        *p_num_allocated_bufs = 0;
         p_buffer = (unsigned char *)&p_pool_pointer[0];
         for(inx=0; inx < pool_size; inx++)
         {
             if(!hxsc_memmgr_is_buffer_free(p_buffer))
             {
               *p_num_allocated_bufs += 1;
             }
             p_buffer = p_buffer + bufsize + HXSC_MEMMGR_BUF_HEADER_SIZE;
         }
    }
    void hxsc_memmgr_get_pool_counts(int poolinx, int *p_bufsize, int *p_pool_size, int *p_num_allocated_bufs)
    {
        const unsigned char  *p_pool;

        *p_bufsize   = ghxsc_memmgr_buf_sizes[poolinx];
        *p_pool_size = ghxsc_memmgr_pool_sizes[poolinx];
        p_pool    = ghxsc_memmgr_pool_pointer[poolinx];
        hxsc_memmgr_count_buffers(p_pool, *p_pool_size, *p_bufsize, p_num_allocated_bufs);
    }

   void hxsc_memmgr_verify_pool_counts(int poolinx, int expected_allocated_bufs)
    {
        int bufsize;
        int pool_size;
        int num_allocated_bufs;

        hxsc_memmgr_get_pool_counts(poolinx,&bufsize, &pool_size, &num_allocated_bufs);
        if(num_allocated_bufs != expected_allocated_bufs)
        {
            HXSC_PRINTF("Error: Expected to have %d availbale buffers in pool %d, but we have %d\n", poolinx+1, expected_allocated_bufs, num_allocated_bufs);
        }
    }

    void hxsc_memmgr_verify_not_null(unsigned char *p_mem)
    {
        if(p_mem == NULL)
        {
            HXSC_PRINTF("Error: The p[ointer expected to be valid\n");
        }
    }
    void hxsc_memmgr_allocate_multiple_buffers(int buff_size, int quantity)
    {
        unsigned char *p_mem;
        int            inx;

        for(inx = 0; inx < quantity; inx++)
        {
            p_mem = hxsc_malloc(buff_size);
            if(p_mem == NULL)
            {
                break;
            }
            memset(p_mem, 0x55, buff_size);
        }
    }
    #define HXSC_MEMMGR_PRINT_STATISTICS()       hxsc_memmgr_print_statistics()
    #define HXSC_MEMMGR_VERIFY_POOL_COUNTS(a,b)  hxsc_memmgr_verify_pool_counts(a,b)
    #define HXSC_MEMMGR_VERIFY_NOT_NULL(p)       if(p == NULL) HXSC_PRINTF("Error: The pointer expected to be valid\n")
    #define HXSC_MEMMGR_VERIFY_NULL(p)           if(p != NULL) HXSC_PRINTF("Error: The pointer expected to be NULL\n")
    #define HXSC_MEMMGR_ALLOC_MULT_BUFFS(a, b)   hxsc_memmgr_allocate_multiple_buffers(a, b)
#else
    #define HXSC_MEMMGR_PRINT_STATISTICS()  
    #define HXSC_MEMMGR_VERIFY_POOL_COUNTS(a,b)  
    #define HXSC_MEMMGR_VERIFY_NOT_NULL(p)       
    #define HXSC_MEMMGR_VERIFY_NULL(p)           
    #define HXSC_MEMMGR_ALLOC_MULT_BUFFS(a, b)
 
#endif

#ifdef yaainteg
int main()
{
    unsigned char *p_mem;

    hxsc_memmgr_init();

    HXSC_MEMMGR_ALLOC_MULT_BUFFS(HXSC_MEMMGR_BUFSIZE1, HXSC_MEMMGR_POOLSIZE1);
    HXSC_MEMMGR_ALLOC_MULT_BUFFS(HXSC_MEMMGR_BUFSIZE2, HXSC_MEMMGR_POOLSIZE2);
    HXSC_MEMMGR_ALLOC_MULT_BUFFS(HXSC_MEMMGR_BUFSIZE3, HXSC_MEMMGR_POOLSIZE3);
    HXSC_MEMMGR_ALLOC_MULT_BUFFS(HXSC_MEMMGR_BUFSIZE4, HXSC_MEMMGR_POOLSIZE4);
    HXSC_MEMMGR_PRINT_STATISTICS();


    //p_mem = hxsc_malloc(HXSC_MEMMGR_BUFSIZE1);
    //HXSC_MEMMGR_VERIFY_NOT_NULL(p_mem);
    //HXSC_MEMMGR_VERIFY_POOL_COUNTS(0, HXSC_MEMMGR_POOLSIZE1);
    //p_mem = hxsc_malloc(HXSC_MEMMGR_BUFSIZE1);
    //HXSC_MEMMGR_VERIFY_NULL(p_mem);
    //
    //HXSC_MEMMGR_PRINT_STATISTICS();

    //p_mem = hxsc_malloc(HXSC_MEMMGR_BUFSIZE1);
    //HXSC_MEMMGR_PRINT_STATISTICS();
    //hxsc_free(p_mem);
    //
    //HXSC_MEMMGR_PRINT_STATISTICS();
	return 0;
}
#endif
